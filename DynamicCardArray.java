import java.util.Random;
public class DynamicCardArray {
	//Fields
	private SingleCard[] cardValues;
	private int next;
	
	//Constructor
	public DynamicCardArray() {
		this.cardValues = new SingleCard[1000];
		this.next = 0;
	}

	//Adds a card in the next available space
	public void add(SingleCard card)
	{
		if(this.next >= this.cardValues.length)
		{
			this.doubleCapacity();
		}
		this.cardValues[this.next] = card;
		this.next++;
	}
	
	//Getter
	public SingleCard[] getCardValues() {
		return cardValues;
	}
	
	//Setter
	public void setCardValues(SingleCard[] cardValues) {
		this.cardValues = cardValues;
	}

	//Removes a card and shifts the array by 1
	public SingleCard playCard(int index)
	{
		SingleCard card = this.cardValues[index];
		for(int y=index; y<this.next; y++)
		{
			this.cardValues[y] = this.cardValues[y+1];
		}
		this.next--;
		return card;
	}
	
	//Returns the length to the current card
	public int getNext()
	{
		return this.next;
	}
	
	//Returns a card at an index
	public SingleCard getCard(int index)
	{
		return this.cardValues[index];
	}
	
	//doubleCapacity() -- increases the size of this.cardValues
	private void doubleCapacity()
	{
		SingleCard[] bigger = new SingleCard[cardValues.length*2];
	
		for(int i=0; i<this.cardValues.length; i++)
		{
			bigger[i] = this.cardValues[i];
		}
		this.cardValues = bigger;
	}
	
	//shuffleAllCards() -- swaps the card randomly
	public void shuffleAllCards()
	{
		Random randGen = new Random();
		for(int i=0; i<this.next; i++)
		 {
			SingleCard temp = this.getCard(i);
			int randomNum = randGen.nextInt(this.next-1); 
			this.cardValues[i] = this.cardValues[randomNum];
			this.cardValues[randomNum] = temp;
		 }
	}
	
	//toString() -- Displays the cards
	public String toString(){
		String result = "";
		for(int i=0; i<this.next; i++)
		{
			result += cardValues[i] + "\n";
		}
		return result;
	}
}


