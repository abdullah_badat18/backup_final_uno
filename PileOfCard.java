public class PileOfCard{
	//field
	private DynamicCardArray pile;
	
	//Constructor
	public PileOfCard ()
	{
		this.pile = new DynamicCardArray();
	}
	
	//Returns the top card from the discard pile
	public SingleCard getTopCard()
	{
		return this.pile.getCard(this.pile.getNext() - 1);
	}
	
	//toString() to Display top card
	public String toString()
	{	
		return "" + this.pile.getCard(this.pile.getNext() - 1);
	}
	
	//Adds a new card on the disacrd pile
	public void setNewCardToPlay(SingleCard oneCard)
	{
		this.pile.add(oneCard);
	}
}