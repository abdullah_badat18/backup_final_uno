public class Player{
	//field
	private DynamicCardArray hands;
	private String playerName;
	
	//Constructor
	public Player(String playerName)
	{
		this.hands = new DynamicCardArray();
		this.playerName = playerName;
	}
	
	//the actions that a player will have -- add(draw) a card to the hand
	public void addCard(SingleCard card)
	{
		this.hands.add(card);
	}
	//Getter
	public String getPlayerName()
	{
		return this.playerName;
	}
	
	//Getter
	public DynamicCardArray getHands()
	{
		return this.hands;	
	}	
	
	//the actions that a player will have -- remove(play) a card from the hand
	public SingleCard playCard(int index)
	{
		return this.hands.playCard(index);
	}
	
	public SingleCard getCard(int index)
	{
		return this.hands.getCard(index);
	}
	
	//toString() -- to display the hands of the players
	public String toString()
	{
		return "\n" + this.playerName + ": \n" + this.hands.toString();
	}
}