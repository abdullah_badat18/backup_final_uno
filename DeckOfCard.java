public class DeckOfCard{
	//field
	private DynamicCardArray deckOfCard;
	
	//Consutructor -- sorts all the cards by color and value
	public DeckOfCard()
	{
		this.deckOfCard = new DynamicCardArray();
		
		for(Color col : Color.values())
		{
			for(Value val : Value.values())
			{
				this.deckOfCard.add(new SingleCard(col,val));
			}
		}
		this.shuffleAllCards();
	}
	
	// to dsiplay the deck of cards
	public String toString()
	{
		return this.deckOfCard.toString();
	}
	
	//removeTopCard() -- it returns the top card from the deck of cards
	public SingleCard removeTopCard()
	{
		return this.deckOfCard.playCard(this.deckOfCard.getNext() -1);
	}
	
	//shuffles all the cards in the deck
	private void shuffleAllCards()
	{
		this.deckOfCard.shuffleAllCards();
	}
	
	//Get DeckOfCards
	public DynamicCardArray getDeckOfCard()
	{
		return this.deckOfCard;
	}
	
	//ReMaking a Deck Of Cards
	public void reset() 
	{
		for(Color col : Color.values())
		{
			for(Value val : Value.values())
			{
				this.deckOfCard.add(new SingleCard(col,val));
			}
		}
		this.shuffleAllCards();
	}
}
