public enum Value {
	ZERO ("0"),
	ONE ("1"),
	TWO ("2"),
	THREE ("3"),
	FOUR ("4"),
	FIVE ("5"),
	SIX ("6"),
	SEVEN ("7"),
	EIGHT ("8"),
	NINE ("9"),
	PW("+2");

	//field
	private final String cardValue;
	
	//Constructor
	private Value(final String cardValue) 
	{
		this.cardValue = cardValue;
	}
	
	//Returns the cardValue
	public String getCardValue() 
	{
		return this.cardValue;
	}
}