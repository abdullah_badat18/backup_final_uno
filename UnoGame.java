import java.util.Scanner;
import java.util.InputMismatchException;

public class UnoGame
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("WELCOME TO UNO GAME!");
		
	  //Create a deck Of Cards
		DeckOfCard deck = new DeckOfCard();
	  
	  //Create Player's Array & Call the makePlayersHand()
		final int NUM_PLAYER = 3;
		Player player = new Player("test");
		Player[] players = new Player[NUM_PLAYER];
		makePlayersHand(players, deck, sc);

	  //Set New Card to Play on Discard Pile
		PileOfCard discardPile = new PileOfCard();
		discardPile.setNewCardToPlay(deck.removeTopCard());
		
	  //Run the Game
		playGame(sc, deck, players, discardPile);	
	}

   //makePlayersHand() -- distributes cards to every player's hand
	private static void makePlayersHand(Player[] players, DeckOfCard deck, Scanner sc) 
	{
		final int CARDS_PER_HAND = 7;
		for (int i = 0; i < players.length; i++) 
		{
			System.out.print("player " + (i + 1) + ", Enter Your Name: ");
			String playerName = sc.next();
			players[i] = new Player(playerName);
			for (int j = 0; j < CARDS_PER_HAND; j++) 
			{
				players[i].addCard(deck.removeTopCard());
			}
		}
	}
	//playGame() -- Runs The full Uno Game
	private static void playGame(Scanner sc, DeckOfCard deck, Player[] players, PileOfCard discardPile) 
	{
	  //Full Game Loop -- Executes Until Winner is Found
		boolean gameOver = false;
		while (!gameOver)
		{
			for (int i = 0; i < players.length; i++) 
			{
				System.out.print(players[i] + "\n");
				System.out.println("Card To Play is: \n" + discardPile + "\n");

			   //While Loop to Allow Player to Choose Options Multiple Times
				boolean turnComplete = false;
				while (!turnComplete) 
				{
					try 
					{
						System.out.println(players[i].getPlayerName() + ", Would You Like to Play a Card (1) or Draw a New Card (2)?");
						int player_choice = getPlayerChoice(sc);
					   //If (1) Is Selected
						if (player_choice == 1) 
						{
							System.out.println("Choose a Card: ");
							int card_Index = sc.nextInt();
							boolean restartLoop = false;
						// While Loop to Verify Card Choice and Also Restart "restartLoop" If Player Chooses To Do So
							while (cardNotCorrect(players, card_Index, i, discardPile)) 
							{
								card_Index = sc.nextInt();
								if (card_Index == -1) 
								{
									restartLoop = true;
									break;
								}
							}
							if (restartLoop) 
							{
								continue;
							}
							checkDraw2Card(players, i, card_Index, discardPile, deck);
							discardPile.setNewCardToPlay(players[i].playCard(card_Index));
						}
					   //If (2) Is Selected
						else
						{
						   //Check if Deck is Empty. if yes, Reset it
							if(resetDeck(deck))
							{
								deck.reset();
							}
							players[i].addCard(deck.removeTopCard());
						}
						turnComplete = true;
					}
					catch (InputMismatchException e)
					{
						System.out.println("Invalid Input! Please Enter a Valid Number. ");
						sc.next();
					}
				}
			   //Checks After Each Players Turn If They Have Won and Ends The Game
				if (checkIfWinning(players)) {
					System.out.println("\n"+players[i].getPlayerName()+", You Are The WINNER! CONGRATULAIONS!!!");
					gameOver = true;
					break;
				}
			}
		}
	}
   //getPlayerChoice() -- Prompt The User to Enter their Choice And Validate it 
	private static int getPlayerChoice(Scanner sc)
	{
		int player_choice = sc.nextInt();
		while (player_choice != 1 && player_choice != 2) 
		{
			System.out.println("Invalid Input! " + "Would You Like to Play a Card (1) or Draw a New Card (2)?");
			player_choice = sc.nextInt();
		}
		return player_choice;
	}
   //cardNotCorrect() --  Validation Conditions
	private static boolean cardNotCorrect(Player[] players, int card_Index, int i, PileOfCard discardPile) {
		if (card_Index == -1) {
			return true;
		}

		if (players[i].getCard(card_Index) == null) {
			System.out.println("Card Does Not Exist, Select Another Card or Press -1 to Choose Another Option.");
			return true;
		}

		if ((players[i].getCard(card_Index).getColor() != discardPile.getTopCard().getColor())
				&& (players[i].getCard(card_Index).getValue() != discardPile.getTopCard().getValue())) {
			System.out.println("Invalid Card! " + "Select Another Card or Press -1 to Choose Another Option.");
			return true;
		}
		return false;
	}
   //checkIfWinning() -- Check if a Player Has Won The Game
	private static boolean checkIfWinning(Player[] players) {
		for (int i = 0; i < players.length; i++) {
			if (players[i].getHands().getCardValues()[0] == null) {
				return true;
			}
		}
		return false;
	}
   //checkDraw2Card() -- Implements the Draw Two(+2) Card so The Next Player Will Have to Draw 2 Cards Extra
	private static void checkDraw2Card(Player[] players, int i, int card_index, PileOfCard discardPile, DeckOfCard deck) {
		if ((players[i].getCard(card_index).getColor() == discardPile.getTopCard().getColor())
				&& (players[i].getCard(card_index).getValue().equals(Value.PW))) {
			int nextPlayer = i;
			if (players.length == i + 1) {
				nextPlayer = 0;
			} else {
				nextPlayer++;
			}
			players[nextPlayer].addCard(deck.removeTopCard());
			players[nextPlayer].addCard(deck.removeTopCard());
		}
	}
   //resetDeck() -- Check if Deck of Card is Empty
	private static boolean resetDeck(DeckOfCard deck)
	{
		if(deck.getDeckOfCard().getCardValues()[0] == null)
		{
			return true;
		}
		return false;
	}
}
