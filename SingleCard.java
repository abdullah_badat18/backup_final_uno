public class SingleCard{
	//fields
	private Color color;
	private Value value;
	
	//Constructor
	public SingleCard(Color color, Value value)
	{
		this.color = color;
		this.value = value;
	}
	
	//Returns the color
	public Color getColor()
	{
		return this.color;
	}
	
	//Returns the value
	public Value getValue()
	{
		return this.value;
	}
	
	//toString() -- Displays a single card
	public String toString()
	{	
		return "++++++++++++++++"+"\n"+"+  " +this.color+ "          +" + "\n"+ "+              +"+ "\n"+ "+              +"+ "\n"+ "+              +"
		+ "\n"+ "+       "+this.value.getCardValue()+"      +"+ "\n" + "+              +" + "\n"+ "+              +"+ "\n"+ 
		"+              +"+ "\n"+ "+          "+this.color+"  +"+"\n"+"++++++++++++++++";
	}
}