public enum Color{
	BL("BL"),
	GR("GR"),
	RD("RD"),
	YL("YL");
	
	//field
	private final String cardColor;
	
	//Constructor
	private Color(final String cardColor) 
	{
		this.cardColor = cardColor;
	}
	
	//Returns the card color
	public String getCardColor() 
	{
		return this.cardColor;
	}
}